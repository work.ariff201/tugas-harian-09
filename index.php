<?php    
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $animal = new Animal("Shaun");
    echo "Nama : " .$animal->name ."<br>";
    echo "Legs : " .$animal->legs ."<br>";
    echo "Cold Blooded : " .$animal->cold_blooded ."<br>";
    echo "<br>";

    $animal = new Frog("Budug");
    echo "Nama : " .$animal->name ."<br>";
    echo "Legs : " .$animal->legs ."<br>";
    echo "Cold Blooded : " .$animal->cold_blooded ."<br>";
    echo "Jump : " .$animal->jump ."<br>";
    echo "<br>";

    $animal = new Ape("Kera Sakti");
    echo "Nama : " .$animal->name ."<br>";
    echo "Legs : " .$animal->legs ."<br>";
    echo "Cold Blooded : " .$animal->cold_blooded ."<br>";
    echo "Yell : " .$animal->yell ."<br>";






?>